$(function(){
    $('.img_box').slick({
        // スライダー設定
        infinite: true,
        accessibility: true,
        dots: true,
        pauseOnHover: false,
        pauseOnDotsHover: false,
        // 自動スクロール関係
        autoplay: true,
        autoplaySpeed: 2000,
        // その他
        // fade: true,
        // cssEase: 'linear'
    })
});